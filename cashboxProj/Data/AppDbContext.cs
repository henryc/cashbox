﻿using cashboxProj.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Data
{
    public class AppDbContext: DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) :base(options){}

        public DbSet<People> People { get; set; }
        public DbSet<Attrib> Attribute { get; set; }

        protected  override void OnModelCreating(ModelBuilder builder) 
        {
            builder.Entity<PeopleAttribute>().HasKey(pt => new { pt.AttributeId, pt.PeopleId });
            builder.Entity<PeopleAttribute>().
                HasOne(pt => pt.People).WithMany(pt => pt.PeopleAttributes).HasForeignKey(p => p.PeopleId);
            builder.Entity<PeopleAttribute>().
                HasOne(pt => pt.Attribute).WithMany(pt => pt.PeopleAttributes).HasForeignKey(p => p.AttributeId);
        }
    }
}
