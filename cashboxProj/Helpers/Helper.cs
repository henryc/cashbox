﻿using AutoMapper;
using cashboxProj.Models;
using cashboxProj.ViewModel.AttributeViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Helpers
{
    public class Helper : Profile
    {
        public Helper()
        {
            CreateMap<Attrib, AttributeViewModel>().ReverseMap();
            CreateMap<CreateAttributeViewModel, Attrib>();
        }

    }
}
