﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Models
{
    public class Attrib
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<PeopleAttribute> PeopleAttributes { get; set; }
    }
}
