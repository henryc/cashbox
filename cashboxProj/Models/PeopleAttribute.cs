﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Models
{
    public class PeopleAttribute
    {
        public int Id { get; set; }
        public int PeopleId { get; set; }

        public int AttributeId { get; set; }

        public People People { get; set; }
        public Attrib Attribute { get; set; }
    }
}
