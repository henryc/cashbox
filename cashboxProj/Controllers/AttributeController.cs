﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using cashboxProj.Models;
using cashboxProj.Services;
using cashboxProj.ViewModel.AttributeViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace cashboxProj.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class AttributeController : ControllerBase
    {
        private readonly IPeopleAttribute _peopleAttribute;
        private IMapper _mapper; 

        public AttributeController(IPeopleAttribute peopleAttribute, IMapper mapper)
        {
            _peopleAttribute = peopleAttribute;
            _mapper = mapper;
        }

        // GET: api/Attribute
        [HttpGet]
        public ActionResult Get()
        {
            var model = _peopleAttribute.Attribute.GetAll();
            var vm = _mapper.Map<List<AttributeViewModel>>(model);
            return Ok(vm);
        }

        // GET: api/Attribute/5
        [HttpGet("{id}", Name = "Get")]
        public ActionResult Get(int id)
        {
            var model = _peopleAttribute.Attribute.GetById(id);
            var vm = _mapper.Map<AttributeViewModel>(model);
            return Ok(vm);
        }

        // POST: api/Attribute
        [HttpPost]
        public ActionResult Post([FromBody] CreateAttributeViewModel cvm)
        {
            try
            {
                var model = _mapper.Map<Attrib>(cvm);
                _peopleAttribute.Attribute.Create(model);
                _peopleAttribute.Save();
                return Ok();
            }
            catch (Exception)
            {

                throw;
            }
        }

        // PUT: api/Attribute/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Attrib vm)
        {
            try
            {
                var model = _peopleAttribute.Attribute.GetById(id);
                var gm = _mapper.Map<AttributeViewModel>(model);
                if (gm == null)
                {
                    return BadRequest(StatusCode(404));
                }
                var putmodel = _mapper.Map<Attrib>(vm);
                _peopleAttribute.Attribute.Update(vm);
                _peopleAttribute.Save();
                return Ok();
            }
            catch (Exception)
            {

                throw;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = _peopleAttribute.Attribute.GetById(id);
                var gm = _mapper.Map<AttributeViewModel>(model);
                if (gm == null)
                {
                    return BadRequest(StatusCode(404));
                }
                _peopleAttribute.Attribute.Delete(model);
                return Ok();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
