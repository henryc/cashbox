﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.ViewModel.AttributeViewModel
{
    public class CreateAttributeViewModel
    {
        public string Name { get; set; }
    }
}
