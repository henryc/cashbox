﻿using cashboxProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Services
{
    public interface IPeople
    {
        List<People> GetAll();
        People GetById(int Id);
        void Create(People people);
        void Update(People people);
        void Delete(People people);

    }
}
