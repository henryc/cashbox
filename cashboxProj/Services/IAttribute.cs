﻿using cashboxProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Services
{
    public interface IAttribute
    {
        List<Attrib> GetAll();
        Attrib GetById(int Id);
        void Create(Attrib attribute);
        void Update(Attrib attribute);
        void Delete(Attrib attribute);

    }
}
