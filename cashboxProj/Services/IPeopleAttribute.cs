﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Services
{
    public interface IPeopleAttribute
    {
        IPeople People { get;}
        IAttribute Attribute { get; }
        void Save();
       // void Update();
    }
}
