﻿using cashboxProj.Data;
using cashboxProj.Models;
using cashboxProj.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Repository
{
    public class PeopleRepo : IPeople
    {
        private readonly AppDbContext _context;
        public PeopleRepo(AppDbContext context)
        {
            _context = context;
        }
        public void Create(People people)
        {
            _context.People.Add(people);
        }

        public void Delete(People people)
        {
            _context.People.Remove(people);
        }

        public List<People> GetAll()
        {
            return _context.People.ToList();
        }

        public People GetById(int Id)
        {
            return _context.People.FirstOrDefault(p => p.Id == Id);
        }

        public void Update(People people)
        {
            _context.People.Update(people);
        }
    }
}
