﻿using cashboxProj.Data;
using cashboxProj.Models;
using cashboxProj.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Repository
{
    public class AttributeRepo : IAttribute
    {
        private readonly AppDbContext _context;
        public AttributeRepo(AppDbContext context)
        {
            _context = context;
        }

        public void Create(Attrib attribute)
        {
            
            _context.Attribute.Add(attribute);
        }

        public void Delete(Attrib attribute)
        {
            _context.Attribute.Remove(attribute);
        }

        public List<Attrib> GetAll()
        {
            return _context.Attribute.ToList();
        }

        public Attrib GetById(int Id)
        {
            return _context.Attribute.FirstOrDefault(a => a.Id == Id);
        }

        public void Update(Attrib attribute)
        {
            _context.Attribute.Update(attribute);
        }
    }
}
