﻿using cashboxProj.Data;
using cashboxProj.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cashboxProj.Repository
{
    public class PeopleAttributeRepo : IPeopleAttribute
    {
        private readonly AppDbContext _context;
        private IPeople _peopleRepo;
        private IAttribute _attributeRepo;

        public PeopleAttributeRepo(AppDbContext context)
        {
            _context = context;
        }
        public IPeople People
        {
            get
            {
                return _peopleRepo ??= new PeopleRepo(_context);
            }
        }

        public IAttribute Attribute 
        {
            get
            {
                return _attributeRepo ??= new AttributeRepo(_context);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
/*        public void Update()
        {
            _context.Update(_context);
        }*/
    }
}
